jQuery(document).ready(function ($) {
  // if(width < 768) {
  // 	$('#header').addClass('header-fixed');
  // }
  if (window.location.hash && window.location.hash == "#signup") {
    // Do stuff that are meant to happen when this hash is present.
    showMailingPopUp("external_signup", false);
  }
  // Header fixed and Back to top button
  // $(window).scroll(function () {
  //   if ($(this).scrollTop() > 70) {
  //     $(".back-to-top").fadeIn("slow");
  //     $("#header").addClass("header-fixed");
  //     document.querySelector("#mobile-nav-toggle i").style.color = "white";
  //   } else {
  //     $(".back-to-top").fadeOut("slow");
  //     $("#header").removeClass("header-fixed");
  //     document.querySelector("#mobile-nav-toggle i").style.color = "black";
  //   }
  // });
  $(".back-to-top").click(function () {
    $("html, body").animate(
      {
        scrollTop: 0,
      },
      1500,
      "easeInOutExpo"
    );
    return false;
  });

  // $('.landing-content--rocket').click(function () {
  //   $('html, body').animate({
  //     scrollTop: '-100vh'
  //   }, 1500, 'easeInOutExpo');
  //   return false;
  // });

  // Initiate the wowjs
  new WOW().init();

  // Initiate superfish on nav menu
  $(".nav-menu").superfish({
    animation: {
      opacity: "show",
    },
    speed: 400,
  });

  // Mobile Navigation
  if ($("#nav-menu-container").length) {
    var $mobile_nav = $("#nav-menu-container").clone().prop({
      id: "mobile-nav",
    });
    $mobile_nav.find("> ul").attr({
      class: "",
      id: "",
    });
    $("body").append($mobile_nav);
    $("body").prepend(
      '<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>'
    );
    $("body").append('<div id="mobile-body-overly"></div>');
    $("#mobile-nav")
      .find(".menu-has-children")
      .prepend('<i class="fa fa-chevron-down"></i>');

    $(document).on("click", ".menu-has-children i", function (e) {
      $(this).next().toggleClass("menu-item-active");
      $(this).nextAll("ul").eq(0).slideToggle();
      $(this).toggleClass("fa-chevron-up fa-chevron-down");
    });

    $(document).on("click", "#mobile-nav-toggle", function (e) {
      $("body").toggleClass("mobile-nav-active");
      $("#mobile-nav-toggle i").toggleClass("fa-times fa-bars");
      $("#mobile-body-overly").toggle();
    });

    $(document).click(function (e) {
      var container = $("#mobile-nav, #mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($("body").hasClass("mobile-nav-active")) {
          $("body").removeClass("mobile-nav-active");
          $("#mobile-nav-toggle i").toggleClass("fa-times fa-bars");
          $("#mobile-body-overly").fadeOut();
        }
      }
    });
  } else if ($("#mobile-nav, #mobile-nav-toggle").length) {
    $("#mobile-nav, #mobile-nav-toggle").hide();
  }

  // Smoth scroll on page hash links
  $('a[href*="#"]:not([href="#"])').on("click", function () {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      if (target.length) {
        var top_space = 0;
        if ($("#header").length) {
          top_space = $("#header").outerHeight();

          if (!$("#header").hasClass("header-fixed")) {
            top_space = top_space - 20;
          }
        }

        $("html, body").animate(
          {
            scrollTop: target.offset().top - top_space,
          },
          1500,
          "easeInOutExpo"
        );

        if ($(this).parents(".nav-menu").length) {
          $(".nav-menu .menu-active").removeClass("menu-active");
          $(this).closest("li").addClass("menu-active");
        }

        if ($("body").hasClass("mobile-nav-active")) {
          $("body").removeClass("mobile-nav-active");
          $("#mobile-nav-toggle i").toggleClass("fa-times fa-bars");
          $("#mobile-body-overly").fadeOut();
        }
        return false;
      }
    }
  });
  //
  var rocket = document.querySelector(".landing-content--rocket");
  var fuel = document.querySelector(".rocket-landing-fuel");
  // var surprise = document.querySelector('.surprise');
  // var planetImg = document.querySelector('.surprise img');
  // show fuel on rocket hover
  rocket.addEventListener("mouseover", function () {
    fuel.style.display = "block";
  });
  // hide fuel when quitting mouse from rocket
  rocket.addEventListener("mouseout", function () {
    fuel.style.display = "none";
  });
  // action when clicking on the rocket
  rocket.addEventListener("click", function () {
    // surprise.style.position = "initial";
    // surprise.style.visibility = "visible";
    // setTimeout(() => {
    if (document.location.hostname.search("jogl.io") !== -1) {
      gtag("event", "click", {
        event_category: "rocket",
        event_label: "rocket launch",
      }); // send event to GA
    }
    $("html").animate(
      {
        scrollTop: "0",
        // scrollTop: '-100vh'
      },
      1500,
      "easeInOutExpo"
    );
    // }, 3000);
    // surprise.style.top = "0!important";
    // surprise.style.transform = "translateY(0)!important";
    // planetImg.src = planetImg.getAttribute('data-src'); // load image
  });

  // Partners slick slider
  $(".partners").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2100,
    arrows: false,
    dots: false,
    pauseOnHover: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 3,
        },
      },
    ],
  });
  // show bio popup when clicking on the about button of the person
  $(".about").click(function () {
    var id = $(this).attr("id");
    $(".popup2").fadeToggle("fast");
    $(".truncate").each(function (i) {
      // loop through the bios
      var bio = $(this).html(); // get the selected person's bio
      if (id.split("-")[1] == i) {
        // get team member id by getting part after the dash (ex takes "8" of "#about-8")
        $(".popup2--content").html(bio); // populate the popup with the selected person's bio
      }
    });
  });
  // close bio when clicking on the cross btn
  $(".close-popup2").click(function () {
    $(".popup2").fadeToggle("fast");
  });

  // function to show only an excerpt for bios, and then having a read more button that reveal the full content
  (function () {
    var showChar = 240; // how many characters we want to show
    var ellipsestext = "...";
    if ($(window).width() > 1000) {
      // on desktop
      $(".about").each(function () {
        $(this).hide(); // hide all about buttons on desktop
      });
      $(".truncate").each(function () {
        // loop through each bio
        var content = $(this).html(); // get bio content
        if (content.length > showChar) {
          var truncated_bio = content.substr(0, showChar);
          var full_bio = content;
          var uri = window.location.pathname;
          var moreText = uri == "/" ? "more" : "plus";
          var lessText = uri == "/" ? "Less" : "Réduire";
          var html = // create new html with truncated bio and full bio
            '<div class="truncate-text" style="display:block">' +
            truncated_bio +
            ellipsestext +
            '<br><a href="" class="moreless more">' +
            moreText +
            '</a></span></div><div class="truncate-text" style="display:none">' +
            full_bio +
            '<a href="" class="moreless less">' +
            lessText +
            "</a></span></div>";
          $(this).html(html);
        }
      });
      // toggle full bio when clicking on "read more"
      $(".moreless").click(function () {
        var thisEl = $(this);
        var cT = thisEl.closest(".truncate-text");
        var tX = ".truncate-text";
        if (thisEl.hasClass("less")) {
          cT.prev(tX).toggle();
          cT.slideToggle();
        } else {
          cT.toggle();
          cT.next(tX).fadeToggle();
        }
        return false;
      });
    } else {
      $(".truncate").each(function () {
        $(this).hide(); // hide "read more" link on mobile
      });
    }
  })();

  // Manage action when opening/closing mailinglist/beta popup
  function showMailingPopUp(value, isBetaPopup) {
    $(".popup").fadeToggle("fast"); // open popup
    if (document.location.hostname.search("jogl.io") !== -1) {
      gtag("event", "click", { event_category: "popup", event_label: value }); // send event to GA
    }
    if ($(".input-group input").is(":checked")) {
      // if input is already checked
      if (!isBetaPopup) {
        document.querySelector(".input-group input").click();
      } // uncheck if we launch Beta popup
    } else {
      if (isBetaPopup) {
        document.querySelector(".input-group input").click();
      } // automatically check "join beta program box" on click
    }
  }

  // document.querySelector("#open-popup").onclick = function() {
  // 	showMailingPopUp("menu join beta", true)
  // 	console.log("popo");
  // };
  var openPopup = document.querySelectorAll("#open-popup");
  for (var i = 0; i < openPopup.length; i++) {
    openPopup[i].addEventListener("click", function () {
      showMailingPopUp("menu join beta", true);
      // if($(window).width() < 768) {
      // 	document.querySelector("#mobile-nav-toggle").click()
      // }
    });
  }
  document.querySelector("#open-popup1").onclick = function () {
    showMailingPopUp("landing mailing list", false);
  };
  // document.querySelector("#open-popup2").onclick = function() {
  // 	showMailingPopUp("footer join beta", true)
  // };
  document.querySelector("#open-popup3").onclick = function () {
    showMailingPopUp("footer mailing list", false);
  };
  // track contact button click
  document.querySelector("#contact-us").onclick = function () {
    gtag("event", "click", {
      event_category: "contact",
      event_label: "contact mail",
    }); // send event to GA
  };
  // track white paper download
  var WhitePaperLinks = document.querySelectorAll(".white-paper-dl");
  for (var i = 0; i < WhitePaperLinks.length; i++) {
    WhitePaperLinks[i].addEventListener("click", function () {
      gtag("event", "download", {
        event_category: "white paper",
        event_label: "download white paper",
      }); // send event to GA
    });
  }
  // manage close-popup event
  $(".close-popup").click(function () {
    $(".popup").fadeToggle("fast");
  });

  // start website at on landing, but still let user be able to scroll up to see a "surprise" content (here the earth banner)
  // function restoreAndSkipContent() {
  //   var hidden = document.querySelector(".surprise");
  //   hidden.classList.add("unhide");
  //   window.scroll(0, hidden.offsetHeight);
  // }
  // restoreAndSkipContent();

  // toggle to hide/display collaborators depending on their type
  $("#collab-type").click(function () {
    if (document.querySelector("#collab-type").checked) {
      document.querySelector(".collab-present").style.display = "flex";
      document.querySelector(".collab-past").style.display = "none";
    } else {
      document.querySelector(".collab-present").style.display = "none";
      document.querySelector(".collab-past").style.display = "flex";
    }
  });

  // force open programModal on page load
  // $("#programModal").modal("show");
});
