var browserSync = require("browser-sync");
var cp = require("child_process");
var gulp = require("gulp");
var sass = require("gulp-sass");
var prefix = require("gulp-autoprefixer");

var jekyll = process.platform === "win32" ? "jekyll.bat" : "jekyll";
var messages = {
  jekyllBuild: '<span style="color: grey">Running:</span> $ jekyll build',
};

var paths = {
  listners: {
    src: ["*.html", "_layouts/*.html", "assets/js/*.js"],
  },
  jekyll: {
    dist: "public/",
  },
  styles: {
    src: "_scss/*.scss",
    dist: "assets/css",
  },
};

gulp.task(jekyllBuild);
gulp.task(
  "jekyllrebuild",
  gulp.series(jekyllBuild, function (cb) {
    browserSync.reload();
    cb();
  })
);
gulp.task(serve);
gulp.task(styles);
gulp.task(watch);
gulp.task(
  "default",
  gulp.series(styles, jekyllBuild, gulp.parallel(serve, watch))
);

/* Build the Jekyll Site*/
function jekyllBuild(done) {
  //   browserSync.notify("building jekyll");
  browserSync.notify(messages.jekyllBuild);
  return cp.spawn(jekyll, ["build"], { stdio: "inherit" }).on("close", done);
}

// function jekyllrebuild() {
//   browserSync.reload();
// }

/* Wait for jekyll-build, then launch the Server */
function serve() {
  browserSync.init({
    server: paths.jekyll.dist,
    notify: true,
  });
}

/* Compile files from _scss into both public/css (for live injecting) and site (for future jekyll builds) */
function styles() {
  return (
    gulp
      .src(paths.styles.src)
      //   .pipe(sass().on("error", sass.logError))
      .pipe(
        sass({
          includePaths: ["scss"],
          onError: browserSync.notify,
        })
      )
      .pipe(
        prefix(["last 15 versions", "> 1%", "ie 8", "ie 7"], { cascade: true })
      )
      .pipe(gulp.dest(paths.jekyll.dist + paths.styles.dist))
      .pipe(browserSync.reload({ stream: true }))
      .pipe(gulp.dest(paths.styles.dist))
  );
}

/* Watch scss files for changes & recompile + Watch html/js files, run jekyll & reload BrowserSync */
function watch() {
  gulp.watch(paths.styles.src, styles);
  gulp.watch(paths.listners.src, gulp.series("jekyllrebuild"));
}
